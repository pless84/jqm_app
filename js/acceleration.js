// Accelerometer code
$('#acc_link').click(function () {
	var accDiv = $("#acc_values");
	navigator.accelerometer.getCurrentAcceleration(onSuccess, onError);
	navigator.accelerometer.watchAcceleration(onSuccess, onError, {frequency: 1000});

	var plot = $.jqplot('acc_values', [new Array()], {
		title: 'Acceleration Data',
		series: [
			{
				yaxis: 'y2axis',
	            label: '',
	            showMarker: false,
	            fill: false,
	            neighborThreshold: 3,
	            lineWidth: 2.2,
	            color: '#0571B6',
	            fillAndStroke: true
			}
			],
			axes: {
				xaxis: {
					renderer: $.jqplot.DateAxisRenderer,
					tickOptions: {
						formatString: '%H:%M:%S'
					},
					numberTicks: 10
				}
			},
			cursor: {
				zoom: false,
				showTooltip: false,
				show: false
			},
			highlighter: {
				useAxesFormatters: false,
				showMarker: false,
				show: false
			},
			grid: {
				gridLineColor: '#333333',
				background: 'transparent',
				borderWidth: 3
			}
		});

	var myData = [];
	var x = (newDate()).getTime() - 101000;
	var y = 1;
	myData.push([x, y]);

	plot.series[0].data = myData;
	plot.resetAxesScale();
	plot.axes.xaxis.numberTicks = 10;
	plot.axes.y2axis.numberTicks = 15;
	plot.replot();

	// Run after successful transaction
	function onSuccess(acc) {
		// accDiv.html(
		// 	'Acc X: ' + acc.x + '<br />' +
		// 	'Acc Y: ' + acc.y + '<br />' +
		// 	'Acc Z: ' + acc.z + '<br />' +
		// 	'Timestamp: ' + acc.timestamp
		// );
		myData.splice(0, 1);
		x = (new Date()).getTime();
		y = acc.x;
		myData.push([x, y]);

		plot.series[0].data = myData;
		plot.resetAxesScale();
		plot.axes.xaxis.numberTicks = 10;
		plot.axes.y2axis.numberTicks = 15;
		plot.replot();
	}

	// Run if we face an error
	function onError() {
		accDiv.html(
			'There was an error trying to ' + 
			'locate your current bearing.'
			);
	}
});