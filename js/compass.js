// Compass code
var headingDiv;

$(document).on("deviceready", function () {
	headingDiv = $("#compass_heading");
	navigator.compass.getCurrentHeading(onSuccess, onError);
	navigator.compass.watchHeading(onSuccess, onError, {frequency: 500});
	
	// Run after successful transaction
	// Let's display the compass data
	function onSuccess(heading) {
		headingDiv.html(
			'Heading: ' + heading.magneticHeading + '&#xb0; ' + 
			convertToText(heading.magneticHeading) + '<br />' + 
			'True Heading: ' + heading.trueHeading + '<br />' +
			'Accuracy: ' + heading.headingAccuracy
		);
				
		// Alter the CSS properties to rotate the rose image
		$(".rose").css({
			"-webkit-transform": 
			"rotate(-" + heading.magneticHeading + "deg)",
		   "transform": 
			"rotate(-" + heading.magneticHeading + "deg)"
		});	
	}

	// Run if we face an error 
	// obtaining the compass data
	function onError() {
		headingDiv.html(
			'There was an error trying to ' + 
			'locate your current bearing.'
		);
	}
	
});

// Accept the magneticHeading value
// and convert into a text representation
function convertToText(mh) {
	var textDirection;
	if (typeof mh !== "number") {
		textDirection = ''; 
	} else if (mh >= 337.5 || (mh >= 0 &&  mh <= 22.5)) {
		textDirection =  'N'; 
	} else if (mh >= 22.5 && mh <= 67.5) {
	   textDirection =  'NE'; 
	} else if (mh >= 67.5 && mh <= 112.5) {
	   textDirection =  'E'; 
	} else if (mh >= 112.5 && mh <= 157.5) {
	   textDirection =  'SE'; 
	} else if (mh >= 157.5 && mh <= 202.5) {
	   textDirection =  'S'; 
	} else if (mh >= 202.5 && mh <= 247.5) {
	   textDirection =  'SW'; 
	} else if (mh >= 247.5 && mh <= 292.5) {
	   textDirection =  'W'; 
	} else if (mh >= 292.5 && mh <= 337.5) {
	   textDirection =  'NW'; 
	} else {
		textDirection =  textDirection;
	}
	return textDirection;
}